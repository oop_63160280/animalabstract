/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class Bat extends Poultry {

    private String nickname;

    public Bat(String nickname) {
        super("Bat", 2);
        this.nickname = nickname;
    }

    public void name() {
        System.out.println("Bat name: " + nickname);
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + nickname + " fly at night");
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + nickname + " eat insects");
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + nickname + " not walking");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + nickname + " sleep with head hanging");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + nickname + " speak");
    }

}
