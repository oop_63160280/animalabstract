/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class Crab extends AquaticAnimal{
    private String nickname;

    public Crab(String nickname) {
        super("Crab");
        this.nickname = nickname;
    }

    public void name(){
        System.out.println("Crab name: "+ nickname);
    }
    
    @Override
    public void swim() {
System.out.println("Crab: "+ nickname + " use your legs to swim");
    }

    @Override
    public void eat() {
System.out.println("Crab: "+ nickname + " like to eat shellfish");
    }

    @Override
    public void walk() {
System.out.println("Crab: "+ nickname + " use your legs to walk");
    }

    @Override
    public void sleep() {
System.out.println("Crab: "+ nickname + " sleep under rocks or shells");
    }

    @Override
    public void speak() {
System.out.println("Crab: "+ nickname + " speak");
    }
    
}
