/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class Bird extends Poultry {

    private String nickname;

    public Bird(String nickname) {
        super("Bird", 2);
        this.nickname = nickname;
    }

    public void name() {
        System.out.println("Bird name: " + nickname);
    }

    @Override
    public void fly() {
        System.out.println("Bird: " + nickname + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird: " + nickname + " eat worms");
    }

    @Override
    public void walk() {
        System.out.println("Bird: " + nickname + " walk on two legs");
    }

    @Override
    public void sleep() {
        System.out.println("Bird: " + nickname + " sleep in the nest");
    }

    @Override
    public void speak() {
        System.out.println("Bird: " + nickname + " speak jibjib");
    }

}
