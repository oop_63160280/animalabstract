/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class Fish extends AquaticAnimal {

    private String nickname;

    public Fish(String nickname) {
        super("Fish");
        this.nickname = nickname;
    }

    public void name() {
        System.out.println("Fish name: " + nickname);
    }

    @Override
    public void swim() {
        System.out.println("Fish: " + nickname + " swim with the tail");
    }

    @Override
    public void eat() {
        System.out.println("Fish: " + nickname + " likes to eat pellets for fish");
    }

    @Override
    public void walk() {
        System.out.println("Fish: " + nickname + " swim forward");
    }

    @Override
    public void sleep() {
        System.out.println("Fish: " + nickname);
    }

    @Override
    public void speak() {
        System.out.println("Fish: " + nickname + " speak bongbong");
    }

}
