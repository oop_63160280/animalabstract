/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class Cat extends LandAnimal{
    private String nickname;

    public Cat(String nickname) {
        super("Cat",4);
        this.nickname = nickname;
    }

    public void name(){
        System.out.println("Dog name: "+ nickname);
    }
    
    @Override
    public void run() {
        System.out.println("Cat: " + nickname + " run ");
    }

    @Override
    public void eat() {
        System.out.println("Cat: " + nickname + " like to eat fish ");
    }

    @Override
    public void walk() {
        System.out.println("Cat: " + nickname + " walk ");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: " + nickname + " sleep on soft cloth ");
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + nickname + " speak meowmeow ");
    }
    
    
}
