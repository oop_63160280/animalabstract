/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Karn");
        System.out.println(h1);
        h1.eat();
        h1.run();
        h1.sleep();
        h1.speak();
        h1.walk();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is Land Animal ? " + (h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is Land Animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile Animal ? " + (a1 instanceof Reptile));
        System.out.println("*************************************");

        Cat c1 = new Cat("Korn");
        System.out.println(c1);
        c1.eat();
        c1.run();
        c1.sleep();
        c1.speak();
        c1.walk();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        Animal b1 = c1;
        System.out.println("b1 is Land Animal ? " + (b1 instanceof LandAnimal));
        System.out.println("b1 is Poultry Animal ? " + (b1 instanceof Poultry));
        System.out.println("b1 is Human ? " + (b1 instanceof Human));
        System.out.println("*************************************");

        Dog d1 = new Dog("Ball");
        System.out.println(d1);
        d1.name();
        d1.speak();
        d1.sleep();
        d1.eat();
        System.out.println("d1 is Land Animal ? " + (d1 instanceof LandAnimal));
        Animal e1 = d1;
        System.out.println("e1 is Land Animal ? " + (e1 instanceof LandAnimal));
        System.out.println("e1 is Human ? " + (e1 instanceof Human));
        System.out.println("*************************************");

        Fish f1 = new Fish("Blue");
        System.out.println(f1);
        f1.name();
        f1.speak();
        f1.eat();
        f1.walk();
        System.out.println("f1 is Aquatic Animal ? " + (f1 instanceof AquaticAnimal));
        Animal g1 = f1;
        System.out.println("g1 is Land Animal ? " + (g1 instanceof LandAnimal));
        System.out.println("g1 is Aquatic Animal ? " + (g1 instanceof AquaticAnimal));
        System.out.println("*************************************");

        Crab ca1 = new Crab("Jojo");
        ca1.name();
        ca1.eat();
        ca1.sleep();
        ca1.walk();
        System.out.println("ca1 is Aquatic Animal ? " + (ca1 instanceof AquaticAnimal));
        System.out.println("ca1 is Animal ? " + (ca1 instanceof Animal));
        Animal q1 = ca1;
        System.out.println("q1 is Land Animal ? " + (q1 instanceof LandAnimal));
        System.out.println("*************************************");

        Bird bi1 = new Bird("Tiffy");
        bi1.name();
        bi1.fly();
        bi1.sleep();
        bi1.speak();
        System.out.println("Bi1 is Poultry Animal ? " + (bi1 instanceof Poultry));
        Animal p1 =bi1;
        System.out.println("p1 is Land Animal ? " + (p1 instanceof LandAnimal));
        System.out.println("p1 is Crab ? " + (p1 instanceof Crab));
        System.out.println("*************************************");
        
        Bat ba1 = new Bat("Raft");
        ba1.name();
        ba1.sleep();
        ba1.eat();
        ba1.walk();
        ba1.speak();
        System.out.println("Ba1 is Poultry Animal ? " + (ba1 instanceof Poultry));
        System.out.println("Ba1 is Animal ? " + (ba1 instanceof Animal));
        System.out.println("*************************************");
        
        Crocodile cr1 = new Crocodile("coco");
        cr1.name();
        cr1.eat();
        cr1.sleep();
        cr1.walk();
        System.out.println("cr1 is Reptile Animal ? " + (cr1 instanceof Reptile));
        System.out.println("cr1 is Animal ? " + (cr1 instanceof Animal));
        Animal y1 = cr1;
        System.out.println("y1 is Poultry Animal ? " + (y1 instanceof Poultry));
        System.out.println("*************************************");

        Snake sn1 = new Snake("Drag");
        sn1.name();
        sn1.crawl();
        sn1.eat();
        sn1.sleep();
        sn1.speak();
        sn1.walk();
        System.out.println("sn1 is Reptile Animal ? " + (sn1 instanceof Reptile));
        System.out.println("sn1 is Animal ? " + (sn1 instanceof Animal));
        Animal s1 = sn1;
        System.out.println("s1 is Poultry Animal ? " + (s1 instanceof Poultry));
        System.out.println("s1 is Land Animal ? " + (s1 instanceof LandAnimal));
        
        
        
        
    }

}
