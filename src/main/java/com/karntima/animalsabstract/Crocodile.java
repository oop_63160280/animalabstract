/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.animalsabstract;

/**
 *
 * @author User
 */
public class Crocodile extends Reptile {

    private String nickname;

    public Crocodile(String nickname) {
        super("Crocodile", 4);
        this.nickname = nickname;
    }

    public void name() {
        System.out.println("Crocodlie name: " + nickname);
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: " + nickname + " crocodile eating fresh meat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile: " + nickname + " slow walk");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: " + nickname + " sleep with open mouth");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: " + nickname + " speak");
    }

}
